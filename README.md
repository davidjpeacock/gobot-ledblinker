# LEDBlinker

This is an example courtesy of [gobot.io](https://gobot.io/) which blinks an
LED on a breadboard attached to a Raspberry Pi.

## Components

* One of the following: Raspberry Pi B, Raspberry Pi 2, or Raspberry Pi 3
* One breadboard
* One LED
* One 330Ω resistor
* Two male-female jumper wires

## Wiring Diagram

![Fritzing Raspberry Pi + Breadboard Diagram](LEDBlinker_bb.png)

## Usage

* `go get -d -u gobot.io/x/gobot/... && go install gobot.io/x/gobot/platforms/raspi`
* `go get github.com/davidjpeacock/gobot-ledblinker`
* `sudo gobot-ledblinker`
